# AWS CDK TypeScript Project

This repository contains a project for deploying infrastructure using AWS Cloud Development Kit (AWS CDK) written in TypeScript. It sets up a network with public and private subnets, deploys EC2 instances including a bastion host, and provisions an S3 bucket.