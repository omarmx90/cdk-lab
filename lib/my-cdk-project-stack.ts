import * as cdk from '@aws-cdk/core';
import * as ec2 from '@aws-cdk/aws-ec2';
import * as s3 from '@aws-cdk/aws-s3';
import * as iam from '@aws-cdk/aws-iam';

export class MyCdkProjectStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // Create a VPC with both public and private subnets
    const vpc = new ec2.Vpc(this, 'MyVPC', {
      subnetConfiguration: [
        {
          name: 'PublicSubnet',
          subnetType: ec2.SubnetType.PUBLIC,
        },
        {
          name: 'PrivateSubnet',
          subnetType: ec2.SubnetType.PRIVATE_ISOLATED,
        },
      ],
    });

    // Create VPC Endpoints for SSM
    vpc.addInterfaceEndpoint('SsmVpcEndpoint', {
      service: ec2.InterfaceVpcEndpointAwsService.SSM,
    });
    vpc.addInterfaceEndpoint('SsmMessagesVpcEndpoint', {
      service: ec2.InterfaceVpcEndpointAwsService.SSM_MESSAGES,
    });
    vpc.addInterfaceEndpoint('Ec2MessagesVpcEndpoint', {
      service: ec2.InterfaceVpcEndpointAwsService.EC2_MESSAGES,
    });

    // Bastion Host Security Group
    const bastionSecurityGroup = new ec2.SecurityGroup(this, 'BastionSecurityGroup', {
      vpc,
      description: 'Allow SSH access to bastion host',
      allowAllOutbound: true,
    });
    bastionSecurityGroup.addIngressRule(ec2.Peer.anyIpv4(), ec2.Port.tcp(22), 'Allow SSH access');

    // Application Server Security Group
    const appSecurityGroup = new ec2.SecurityGroup(this, 'AppSecurityGroup', {
      vpc,
      description: 'Allow HTTP and SSH access from bastion host',
      allowAllOutbound: true,
    });
    appSecurityGroup.addIngressRule(bastionSecurityGroup, ec2.Port.tcp(80), 'Allow HTTP access from the bastion host');
    appSecurityGroup.addIngressRule(bastionSecurityGroup, ec2.Port.tcp(22), 'Allow SSH access from the bastion host');

    // Define IAM Role for SSM
    const ssmRole = new iam.Role(this, 'SSMRole', {
      assumedBy: new iam.ServicePrincipal('ec2.amazonaws.com'),
      managedPolicies: [
        iam.ManagedPolicy.fromAwsManagedPolicyName('AmazonSSMManagedInstanceCore'),
        iam.ManagedPolicy.fromAwsManagedPolicyName('CloudWatchAgentServerPolicy') // Optional: for CloudWatch integration
      ],
    });

    // UserData to add SSH key and ensure SSM agent is active
    const publicKey = process.env.SSH_PUBLIC_KEY; // Ensure this environment variable is set in your CI/CD settings
    const userData = ec2.UserData.forLinux();
    userData.addCommands(
      'mkdir -p /home/ec2-user/.ssh',
      `echo "${publicKey}" >> /home/ec2-user/.ssh/authorized_keys`,
      'chmod 700 /home/ec2-user/.ssh',
      'chmod 600 /home/ec2-user/.ssh/authorized_keys',
      'chown -R ec2-user:ec2-user /home/ec2-user/.ssh',
      'sudo systemctl enable amazon-ssm-agent',
      'sudo systemctl start amazon-ssm-agent'
    );

    // Bastion Host EC2 instance
    const bastion = new ec2.Instance(this, 'BastionHost', {
      vpc,
      instanceType: ec2.InstanceType.of(ec2.InstanceClass.T2, ec2.InstanceSize.NANO),
      machineImage: ec2.MachineImage.latestAmazonLinux(),
      securityGroup: bastionSecurityGroup,
      keyName: 'cdk-lab',
      vpcSubnets: { subnetType: ec2.SubnetType.PUBLIC },
      userData: userData,
    });

    // Application Server EC2 instance
    const appServer = new ec2.Instance(this, 'AppServer', {
      vpc,
      instanceType: ec2.InstanceType.of(ec2.InstanceClass.T2, ec2.InstanceSize.MICRO),
      machineImage: ec2.MachineImage.latestAmazonLinux(),
      securityGroup: appSecurityGroup,
      vpcSubnets: { subnetType: ec2.SubnetType.PRIVATE_ISOLATED },
      userData: userData,
      role: ssmRole,
    });

    // S3 Bucket
    new s3.Bucket(this, 'Omarmx90Bucket', {
      removalPolicy: cdk.RemovalPolicy.DESTROY,
    });

    // Outputs for connecting to the instances
    new cdk.CfnOutput(this, 'BastionHostIP', { value: bastion.instancePublicIp });
    new cdk.CfnOutput(this, 'AppServerPrivateIP', { value: appServer.instancePrivateIp });
  }
}
